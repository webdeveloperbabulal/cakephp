<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?= __('Edit Group') ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?= __('Edit Group') ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!--  column -->
        <div class="col-md-12">
          
          <!-- general form elements disabled -->
          <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title"><?= __('Edit Group') ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <?= $this->Form->create($group) ?>
                <div class="form-group">
                  <?php echo $this->Form->control('name', ['class'=>'form-control','div'=>false]); ?>
                </div>
                <div class="form-group">
                  <?= $this->Form->button(__('Submit'),['class'=>'btn']) ?>
                </div>
              <?= $this->Form->end() ?>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->