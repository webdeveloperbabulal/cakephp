<?php $this->layout = 'login'; ?>

  <?= $this->Form->create() ?>
  <div class="form-group has-feedback">
    <?= $this->Form->input('username',['placeholder'=>'E-mail','class'=>'form-control','div'=>false,'label'=>false]) ?>
    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
  </div>
  <div class="form-group has-feedback">
    <?= $this->Form->input('password',['placeholder'=>'Password','class'=>'form-control','div'=>false,'label'=>false]) ?>
    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
  </div>
  <div class="row">
    <div class="col-xs-8">
      <div class="checkbox icheck">
        <label>
          <input type="checkbox"> Remember Me
        </label>
      </div>
    </div>
    <!-- /.col -->
    <div class="col-xs-4">
      <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
    </div>
    <!-- /.col -->
  </div>
<?= $this->Form->end() ?>
