<?php
use Cake\Core\Configure;

$file = Configure::read('Theme.folder'). DS . 'src' . DS . 'Template' . DS . 'Element' . DS . 'aside' . DS . 'sidebar-menu.ctp';
if (file_exists($file)) {
    ob_start();
    include_once $file;
    echo ob_get_clean();
} else {
?>
<ul class="sidebar-menu">
    <li class="header">MAIN NAVIGATION</li>
    <li class="treeview">
        <?= $this->Html->link(__('<i class="fa fa-dashboard"></i> <span>Dashboard</span>'), ['controller' => 'Users', 'action' => 'dashboard'], ['escape'=>false]) ?>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-users"></i>
            <span>Users</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li><?= $this->Html->link(__('<i class="fa fa-circle-o"></i>Add User'), ['controller' => 'Users', 'action' => 'add'], ['escape'=>false]) ?></li>
            <li><?= $this->Html->link(__('<i class="fa fa-circle-o"></i>List Users'), ['controller' => 'Users', 'action' => 'index'], ['escape'=>false]) ?></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-users"></i>
            <span>Role Groups</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li><?= $this->Html->link(__('<i class="fa fa-circle-o"></i>Add Group'), ['controller' => 'Groups', 'action' => 'add'], ['escape'=>false]) ?></li>
            <li><?= $this->Html->link(__('<i class="fa fa-circle-o"></i>List Groups'), ['controller' => 'Groups', 'action' => 'index'], ['escape'=>false]) ?></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-users"></i>
            <span>Roles</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li><?= $this->Html->link(__('<i class="fa fa-circle-o"></i>Add Role'), ['controller' => 'Roles', 'action' => 'add'], ['escape'=>false]) ?></li>
            <li><?= $this->Html->link(__('<i class="fa fa-circle-o"></i>List Roles'), ['controller' => 'Roles', 'action' => 'index'], ['escape'=>false]) ?></li>
        </ul>
    </li>
    
    
</ul>
<?php } ?>
